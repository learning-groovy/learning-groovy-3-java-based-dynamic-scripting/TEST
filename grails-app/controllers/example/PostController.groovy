package example

import grails.converters.JSON
import grails.converters.XML

class PostController {
    def getPosts() {
        render Post.list() as JSON
    }
    def getPostsXML() {
        render Post.list() as XML
    }
}
