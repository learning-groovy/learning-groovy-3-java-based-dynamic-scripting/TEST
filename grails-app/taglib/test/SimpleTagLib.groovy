package test

import java.text.SimpleDateFormat

class SimpleTagLib {
    static defaultEncodeAs = [taglib:'html']
    static namespace = "my" // changes from g: namespace to my:
    def dateFormat = { attrs, body ->
        out << new SimpleDateFormat(attrs.format as String).format(attrs.date)
    }
}
